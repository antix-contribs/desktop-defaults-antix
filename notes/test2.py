#!/usr/bin/env python
import pygtk
pygtk.require('2.0')
import gtk

class PyApp(gtk.Window):
    def __init__(self):
        super(PyApp, self).__init__()
        self.set_title("TreeView with ListStore")
        self.set_default_size(250, 200)
        self.set_position(gtk.WIN_POS_CENTER)
  
        store = gtk.ListStore(str,str)
        store.append (["PyQt","item1"])
        store.append (["Tkinter","item1"])
        store.append (["WxPython","item1"])
        store.append (["PyGTK","item1"])
        store.append (["PySide","item1"])
      
        treeView = gtk.TreeView()
        treeView.set_model(store)
        
        for i, column_title in enumerate(["Python GUI Libraries", "test"]):
            renderer = gtk.CellRendererText()
            column = gtk.TreeViewColumn(column_title, renderer, text=i)
            column.set_resizable(True)
            column.set_visible(True)
            treeView.append_column(column)
      
        fixed = gtk.Fixed()
        lbl = gtk.Label("select a GUI toolkit")
        fixed.put(lbl, 25,75)
        fixed.put(treeView, 125,15)

        lbl2 = gtk.Label("Your choice is:")
        fixed.put(lbl2, 25,175)
        self.label = gtk.Label("")
  
        fixed.put(self.label, 125,175)
        self.add(fixed)
      
        treeView.connect("row-activated", self.on_activated)
        self.connect("destroy", gtk.main_quit)
        self.show_all()

    def on_activated(self, widget, row, col):
      
      model = widget.get_model()
      text = model[row][0]
      self.label.set_text(text)

def main():
    gtk.main()
    return

if __name__ == "__main__":
    bcb = PyApp()
    main()
