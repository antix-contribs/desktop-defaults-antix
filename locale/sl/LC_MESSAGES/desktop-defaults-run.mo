��          4      L       `   %   a   X   �   �  �   0   �  R   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 16:52+0300
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2021
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.3
 Potrebno je določiti program, ki naj se zažene Program, ki bi se zagnal sem jaz sam.\nIzbrati morate nekoga drugega, kot sem jaz. 