��          4      L       `   %   a   X   �   �  �   5   r  c   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 16:51+0300
Last-Translator: Paulo Carvalho <paulop1976@gmail.com>, 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 É necessário especificar uma aplicação a executar Este é o próprio programa a ser executado. \n É necessário selecionar outro programa diferente. 