��          4      L       `   %   a   X   �   v  �   2   W  _   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 16:44+0300
Last-Translator: Pedro Delgado, 2021
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Necesita especificar una aplicación para ejecutar El programa que está iniciando es yo mismo. \n Deberá seleccionar un programa que no sea yo.  