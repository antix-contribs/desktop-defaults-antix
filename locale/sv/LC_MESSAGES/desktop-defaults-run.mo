��          4      L       `   %   a   X   �   �  �   *   n  R   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 16:53+0300
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2021
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Du måste specifiera ett program att köra Programmet som startas är detta. \n Du måste välja ett annat program än detta. 