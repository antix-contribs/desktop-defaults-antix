��          4      L       `   %   a   X   �   �  �   '   s  b   �                    Need to specify an application to run The program being started is myself. \n You will need to select a program other than me. Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-04-19 16:48+0300
Last-Translator: Davide Carli <dede.carli.drums@gmail.com>, 2021
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Specificare un'applicazione da eseguire Il programma che sta per essere avviato sono io. \n Dovrai selezionare un programma diverso da me. 